#!/usr/bin/env python3

"""
Simulating and testing selected assembly programs against reference values.
"""

import argparse
import filecmp
import importlib.util
import os
import stat
import subprocess
import sys
import warnings
from pathlib import Path

import numpy as np

import config

# DEBUG
import pdb

def main():
	# Config
	config.init(root_search_path=os.path.abspath(__file__))
	config.cp.read(config.CFG_FILE)
	Nexp_min = int(config.cp['Limit_values']['nexp_min'])
	Nexp_max = int(config.cp['Limit_values']['nexp_max'])
	config.set_dir('SHARE', config.ROOT_DIR, config.cp['Paths']['share_dir'])
	config.set_dir('PY_REF', config.ROOT_DIR, config.cp['Paths']['py_ref_dir'])
	config.set_dir('TCE', config.ROOT_DIR, config.cp['Paths']['tce_dir'])
	config.set_dir('DUMP', config.ROOT_DIR, config.cp['Paths']['dump_dir'])
	config.set_dir('DIFF', config.ROOT_DIR, config.cp['Paths']['diff_dir'])
	config.set_dir('ASM', config.ROOT_DIR, config.cp['Paths']['asm_dir'])
	config.set_dir('ADF', config.ROOT_DIR, config.cp['Paths']['adf_dir'])
	config.set_dir('TPEF', config.ROOT_DIR, config.cp['Paths']['tpef_dir'])
	config.set_dir('TMP', config.ROOT_DIR, config.cp['Paths']['tmp_dir'])
	config.set_dir('SCRIPT', config.ROOT_DIR, config.cp['Paths']['script_dir'])
	sys.path.append(str(config.DIRS['SHARE']))
	# Set up possible simulation targets
	files = {}
	config.set_dir('TGT', config.ROOT_DIR, config.cp['Targets']['targets_dir'])
	files['targets'] = config.DIRS['TGT'] \
	                     .joinpath(config.cp['Targets']['targets_file'])
	config.cp_tgt.read(files['targets'])
	targets = ['clear']
	for section in config.cp_tgt.sections():
		targets.append(section)
	# Argument parsing
	parser = argparse.ArgumentParser(
	           description = __doc__,
	           formatter_class = argparse.ArgumentDefaultsHelpFormatter)
	parser.add_argument('tgt', type=str,
	                    choices=targets, nargs="?",
	                    help="Testing target. The list is automaticly generated\
	                          from your targets conf file. 'clear' deletes all\
	                          temporary files created by reftest.")
	parser.add_argument('--keep-tmp', action='store_true', default=False,
	                    help="Keep temporary files.")
	parser.add_argument('-n', type=int, default=Nexp_max,
	                    help="From FFT size 2^nexp_min compute until 2^'n'.")
	parser.add_argument('-p', '--precision', type=int, default=0,
	                    help="Precision of diff files. If the difference\
	                          between dump and ref is lower then this number,\
	                          output will be suppressed. The value is in Q15\
	                          fixed point format written as decimal\
	                          (e.g. '-p 20' corresponds to 0x0014 fp value).")
	parser.add_argument('-m', '--mau-size', type=int, default=8,
	                    help="Size of MAU (minimum addressable unit) in bits.")
	parser.add_argument('-w', '--word-length', type=int, default=32,
	                    help="Length of one word in bits.")
	args = parser.parse_args()

	# Arguments checks
	if not args.tgt:
		parser.print_help()
		sys.exit(2)

	if args.tgt == "clear":
		rm_list = [ config.ROOT_DIR.joinpath("cmds"),
		            config.ROOT_DIR.joinpath("tmp") ]
		for file_or_dir in rm_list:
			try:
				unlink_all(file_or_dir)
			except FileNotFoundError:
				pass
		sys.exit(0)

	if args.n not in range(Nexp_min, Nexp_max+1):
		raise ValueError("Maximum Nexp must be within [{}, {}]"
		                    .format(Nexp_min, Nexp_max))

	if args.word_length % args.mau_size:
		raise ValueError("Invalid MAU or word length. "
		                 "Word needs to contain a whole number of MAUs.")

	# Default filenames
	files['asm']       = config.DIRS['ASM']\
	                       .joinpath("test_{}.tceasm".format(args.tgt))
	files['adf']       = config.DIRS['ADF']\
	                       .joinpath("minimal_fft.adf")
	files['tpef']      = config.DIRS['TPEF']\
	                       .joinpath("test_{}.tpef".format(args.tgt))
	files['asm_tmp']   = config.DIRS['TMP']\
	                       .joinpath("test_{}_tmp.tceasm".format(args.tgt))
	files['script']    = config.DIRS['SCRIPT']\
	                       .joinpath("dump_cmd.sh")
	files['incmd']     = config.DIRS['SCRIPT']\
	                       .joinpath("input_command")
	files['ref_base']  = config.DIRS['PY_REF']\
	                       .joinpath(args.tgt)\
	                       .joinpath("ref_{}_".format(args.tgt))
	files['dump_base'] = config.DIRS['DUMP']\
	                       .joinpath(args.tgt)\
	                       .joinpath("dump_{}_".format(args.tgt))
	files['diff_base'] = config.DIRS['DIFF']\
	                       .joinpath(args.tgt)\
	                       .joinpath("diff_{}_".format(args.tgt))
	# Target-specific overrides
	desc = config.cp_tgt[args.tgt]['desc']
	print("Testing {}.\n".format(desc.strip('"\'')))
	Nexp_line = {'num': int(config.cp_tgt[args.tgt]['nexp_line_num']),
	             'text': config.cp_tgt[args.tgt]['nexp_line_text']}
	for option in config.cp_tgt[args.tgt]:
		if "_file" in option:
			previous_file = files[option[:-5]]
			files[option[:-5]] = previous_file.parent \
			                       .joinpath(config.cp_tgt[args.tgt][option])

	# Create a shell ttasim scripdesct
	gen_sim_script(files['script'], files['adf'], files['tpef'])

	# Memory address scaling for different MAU/word lengths
	mau_factor = int(args.word_length / args.mau_size)

	# Print initial info
	print("MAU: {}b; word: {}b\n".format(args.mau_size, args.word_length))
	#print("word: {} bits\n".format(args.wordl))
	print("Architecture file:\n{}".format(files['adf']))
	print("Source file:\n{}".format(files['asm']))
	print("Program file:\n{}".format(files['tpef']))
	print("Simulation output folder:\n{}\n"
	        .format(config.DIRS['DUMP'].joinpath(args.tgt)))
	print("Starting simulations...\n")

	# Simulation loop
	for Nexp in range(Nexp_min, args.n+1):
		# Complete filenames
		filenames = resolve_filenames(Nexp, [ files['ref_base'],
		                                      files['dump_base'],
		                                      files['diff_base']])
		files.update(zip(['ref', 'dump', 'diff'], filenames))
		# Create and modify temporary asm file
		cp_rep_line(files['asm'], files['asm_tmp'], Nexp_line['num'],
		            rep_line(Nexp, Nexp_line['text']),
		            keep_old=args.keep_tmp, old_dir=config.DIRS['TMP'])
		# Compile the temporary asm against 'files['adf']'
		print("Compiling... (Nexp = {})".format(Nexp))
		subprocess.run(['tceasm', '-o',
		               files['tpef'], files['adf'], files['asm_tmp']])
		# Modify the input command for the simulator
		with open(files['incmd'], 'w') as wf:
			wf.write(incmd_line(Nexp, args.tgt, mau_factor))
		#cp_rep_line(files['incmd'], files['incmd'], 0,
		#            incmd_line(Nexp, args.tgt),
		#            keep_old=args.keep_tmp, old_dir=config.DIRS['TMP'])
		print("Simulating...", end=' ')
		# Simulate with ttasim and dump the memory
		subprocess.run([files['script'], files['dump']])
		# Convert memory dump files into one-column files
		lines2col(files['dump'], files['dump'],
		          args.mau_size, args.word_length,
		          keep_old=args.keep_tmp, old_dir=config.DIRS['TMP'])
		print("done. Written to '{}'.".format(files['dump'].name))

	# Comparison loop
	print("\nComparing with Python reference...\n")
	for Nexp in range(Nexp_min, args.n+1):
		# Complete filenames
		filenames = resolve_filenames(Nexp, [ files['ref_base'],
		                                      files['dump_base'],
		                                      files['diff_base']])
		files.update(zip(['ref', 'dump', 'diff'], filenames))
		print("Nexp = {}:".format(Nexp), end=' ')
		# Compare with Python reference and save diff
		diff_num = cmp_files(files['ref'], files['dump'], files['diff'],
		                     args.tgt, args.precision)
		evaluate(diff_num, files['diff'])

	# Delete tmp_asm file
	if not args.keep_tmp:
		files['asm_tmp'].unlink()
	# Clear empty directories in share/tce
	clear_dirs(config.DIRS['TCE'])

def gen_sim_script(script_file, adf_file, tpef_file):
	in_cmd_file = script_file.parent.joinpath("input_command")
	args = [ 'ttasim', '-a', str(adf_file),
	                   '-p', str(tpef_file),
	                   '<', str(in_cmd_file), '>', '$1' ]
	with open(script_file, 'w') as wf:
		wf.write("#!/bin/bash\n")
		wf.write("# Automatically generated simulation script.\n\n")
		wf.write(" ".join(args))
	script_file.chmod(0o777)

def resolve_filenames(Nexp, filename_bases):
	"""Append the Nexp information in the end of each file in 'filename_bases'.

	Direcroties are created if they don't exist.
	"""

	try:
		filenames = []
		for filename_base in filename_bases:
			filename_dir = config.ensure_path(filename_base, True)
			filenames.append(filename_dir.joinpath(
			                   filename_base.name + "{:02d}".format(Nexp)))
	except TypeError:
		filename_dir = config.ensure_path(filename_base, True)
		filenames = filename_dir.joinpath(
		                           filename_bases.name + "{:02d}".format(Nexp))
	return filenames

def cp_rep_line(readfile, writefile, line_num, rep_line,
                silent=True, keep_old=True, old_dir=''):
	"""Copy 'readfile' line by line into 'writefile' and replace a line number
	'line_num' by 'rep_line'.

	If 'readfile' and 'writefile' are the same, 'readfile' is renamed to
	'readfile_old'.

	Parameters
	----------
	readfile : str
		Input file to be read.
	writefile : str
		Output file to be written to.
	line_num : str
		Number of the line which will be replaced. (First line is 0.)
	rep_line : str
		String to be written to 'writefile' on the 'line_num'-th line.
	silent : bool
		If True, suppress printing about renaming when changing the same file.
	keep_old : bool
		If False, delete the '_old' file (case of 'readfile' = 'writefile').
	old_dir : str, Path object
		Directory where to put the '_old' file.

	Returns
	-------
	int : 1 - successful, 0 - unsuccessful
	"""
	if not isinstance(readfile, Path):
		readfile = Path(readfile)
	if not isinstance(writefile, Path):
		writefile = Path(writefile)

	# If changing the same file change the readfile name to 'readfile_old'
	if str(readfile) == str(writefile):
		oldfile = Path(old_dir).joinpath(readfile.name + "_old")
		readfile.rename(oldfile)
		if not silent:
			print("Renaming {} to {}".format(str(readfile), str(oldfile)))
		infile = Path(oldfile)
	else:
		oldfile = None
		infile = Path(readfile)

	# Decode escape characters and strip ' and " from the beginning/end
	rep_line = bytes(rep_line, "utf-8").decode("unicode_escape").strip('"\'')

	with open(infile, 'r') as rf, open(writefile, 'w') as wf:
		for i, line in enumerate(rf):
			if i == int(line_num):
				try:
					wf.write(rep_line)
				except:
					wf.close()
					writefile.unlink()
					warnings.warn("Unexpected error: File: {}, Line: {}."\
					                .format(infile, line_num))
					if oldfile:
						infile.rename(readfile)
					return 0
			else:
				wf.write(line)
	if not keep_old:
		try:
			oldfile.unlink()
		except (FileNotFoundError, AttributeError):
			pass
	return 1

def rep_line(Nexp, line):
	"""Updated line to be replaced in the tceasm file.
	"""

	return line.format(Nexp)

def incmd_line(Nexp, tgt, mau_factor):
	"""ttasim command for dumping memory based on current Nexp and sim. target.
	"""

	N = 2**Nexp
	if tgt == 'out':
		size = N
	else:
		total_stages = int((Nexp+1)/2)
		size = total_stages * N
	size = size * mau_factor;
	return "x /a IODATA /n {} /u b 0".format(size)

def lines2col(readfile, writefile, mau_size, word_length, sep=' ',
	          keep_old=True, old_dir=''):
	"""Converts lines of values in 'readfile' separated by 'sep' into columns
	and saves it into 'writefile'

	Empty lines in 'readfile' are skipped. In case 'in-' and 'out-' files are
	the same, 'file' is copied to 'file_tmp' and output is written to 'file'.
	Optionally these '_tmp' files can be saved.

	Parameters
	----------
	readfile : str
		Input file name.
	writefile : str
		Output file name.
	sep : str
		Separator between values in one line (default: ' ' (space)).
	keep_old : bool
		Keep the '_old' file in case readfile and writefile are the same.
	old_dir : str, Path object
		Directory where to put the '_old' file.
	"""

	if not isinstance(readfile, Path):
		readfile = Path(readfile)

	if str(readfile) == str(writefile):
		oldfile = Path(old_dir).joinpath(readfile.name + "_old")
		readfile.rename(oldfile)
		infile = Path(oldfile)
	else:
		oldfile = None
		infile = Path(readfile)
	with open(infile, 'r') as rf, open(writefile, 'w') as wf:
		for line in rf:
			if line.isspace():
				pass
			else:
				mau_factor = int(word_length / mau_size);
				for chunk in split_each_n(line.strip(), sep, n=mau_factor):
					num = word_from_maus(chunk, mau_size, word_length)
					wf.write("{:#010x}\n".format(num))
	if not keep_old:
		os.remove(oldfile)

def split_each_n(string, sep=None, maxsplit=-1, n=1):
	split_list = string.split(sep, maxsplit)
	for i in range(0, len(split_list), n):
		yield split_list[i:i + n]

def word_from_maus(chunk, mau_size, word_length):
	"""Chunk is a list of MAUs; higher-bit MAUs first.
	"""
	if word_length % mau_size:
		raise ValueError("Invalid MAU or word length. "
		                 "Word needs to contain a whole number of MAUs.")
	mau_factor = int(word_length / mau_size)
	if len(chunk) != mau_factor:
		raise ValueError("Provided MAUs do not match the word length. "
		                 "MAUs: {}, word length: {} bits"
		                  .format(chunk, word_length))
	word = 0
	for mau in chunk:
		mau = int(mau, 16)
		mau_factor -= 1
		word += (mau << mau_size * (mau_factor))
	return word


def norm_file(infile, outfile, delete_tmp=True):
	"""Normalize all values in the file according to the biggest absolute value
	of either real or imaginary part.

	The function assumes that only one value is on one line.
	"""

	if infile == outfile:
		newname = infile + '_tmp'
		os.rename(infile, newname)
		infile = str(newname)

	with open(infile, 'r') as rf:
		raw = rf.read()

	numbers = [ np.uint32(int(val, 16)) for val in raw.split('\n') if val ]
	cplx_vals    = fixedconv.fixed2cplx(numbers)
	maxval       = np.max(np.abs([np.real(cplx_vals), np.imag(cplx_vals)]))
	cplx_norm    = np.divide(cplx_vals, maxval)
	fp_vals_norm = fixedconv.cplx2fixed(cplx_norm)
	with open(outfile, 'w') as wf:
		for num in fp_vals_norm:
			wf.write('{:#010x}\n'.format(num))
	if delete_tmp:
		os.remove(infile)

def cmp_files(file1, file2, outfile, fu, precision=0):
	"""Compare 'file1' and 'file2'. Store diff with line numbers to 'outfile'.

	Intended for files with short lines (i.e. output values in a column).
	Function removes all empty lines from both files between comparison.

	Parameters
	----------
	file1 : str
		Name of the first file with path.
	file2 : str
		Name of the second file with path.
	outfile : str
		Name of the output file with path.
	precision : int
		Diff generation threshold (in Q15 fixed point).

	Returns
	-------
	diff_num : int
		Number of differences between the files.
	"""

	import fpconv
	diff_num = 0
	file1name = file1.name
	file2name = file2.name
	with open(file1, 'r') as f1, open(file2, 'r') as f2:
		lines1 = list(f1)
		lines2 = list(f2)

	lines1 = remove_all(lines1, '\n')
	lines2 = remove_all(lines2, '\n')
	if len(lines2) > len(lines1):
		longer  = lines2
		shorter = lines1
	else:
		longer  = lines1
		shorter = lines2

	output = []
	for i in range(len(shorter)):
		fp1 = int(lines1[i], 16)
		fp2 = int(lines2[i], 16)
		if 'out' in file1name:
			diff = fpconv.fixed2cplx(fp1) - fpconv.fixed2cplx(fp2)
		else:
			diff = fp1 - fp2
		maxval = np.max(np.abs([np.real(diff), np.imag(diff)]))
		if fpconv.dec2fixed(maxval) > precision:
			diff_num += 1
			output.append("{:6d} {} {}    {:#010x}\n".format(
			                                  i,
			                                  lines1[i].strip().rjust(13),
			                                  lines2[i].strip().rjust(13),
			                                  fpconv.cplx2fixed(diff)))
	for j in range(len(longer)-len(shorter)):
		try:
			if longer == lines1:
				output.append("{:6d} {} {}\n"
				               .format(i+j+1,
				                       lines1[i+j+1].strip().rjust(13),
 				                       '   '.ljust(13, '.')))
			else:
				output.append("{:6d} {} {}\n"
				               .format(i+j+1,
				                       '   '.ljust(13, '.'),
				                       lines2[i+j+1].strip().rjust(13)))
			diff_num += 1
		except:
			break
	# Print to file
	if diff_num > 0:
		with open(outfile, 'w') as wf:
			wf.write("{} {} {} {}\n".format('idx'.rjust(6),
			                             file1name.rjust(13),
			                             file2name.rjust(13),
			                             "ref - dump".rjust(13)))
			wf.write("{}-{}-{}-{}\n".format(''.center(6, '-'),
			                                ''.center(13, '-'),
			                                ''.center(13, '-'),
			                                ''.center(13, '-')))
			for line in output:
				wf.write(line)
			wf.write("\n{} mismatches".format(diff_num))
			wf.write("\nprecision: {:#06x} ({:e})".format(precision,
			                                            precision*2**(-15)))
	return diff_num

def remove_all(source, item):
	"""Removes all 'items' from a list 'source'.

	Parameters
	----------
	source : list
		Data from where to remove 'items'.
	item : list item
		Item to be removed.

	Returns
	-------
	source : list
		List without all 'items'.
	"""

	while True:
		try:
			source.remove(item)
		except:
			break
	return source

def evaluate(diff_num, diff_file):
	if diff_num == 0:
		print("Success!", end=' ')
		# Try to delete previous diff file
		try:
			diff_file.unlink()
			print("(Removed previous diff file.)")
		except FileNotFoundError:
			print("")
	else:
		print("Failure! {:d} mismatches. Diff file saved to:"
		  .format(diff_num))
		print("{}".format(diff_file))

def clear_dirs(start_dir):
	"""Remove all empty subdirectories in start_dir (including start_dir).
	"""

	removed = []
	glob_list = sorted(start_dir.glob('**'))
	for path in reversed(glob_list):
		try:
			path.rmdir()
			removed.append(path)
		except (OSError, NotADirectoryError):
			pass
	return removed

def unlink_all(path):
	"""Remove a file or a directory (empty or not) at 'path'.
	"""

	if not isinstance(path, Path):
		path = Path(path)

	if path.is_dir():
		for sub in path.iterdir():
			if sub.is_dir():
				unlink_all(sub)
			else:
				sub.unlink()
		path.rmdir()
	else:
		path.unlink()

if __name__ == "__main__":
	main()
