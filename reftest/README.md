# reftest/reftest #

reftest program source code

[//]: # (tree)
## Directory tree ##
~~~~
/home/kubouch/git/thesis/fft-in-tce/reftest/reftest
├── config.py    // Parsing 'config.txt' and configuring the program
├── __init__.py  // Empty (indicates reftest is a Python module)
├── __main__.py  // Allows running from command line
├── README.md
└── reftest.py   // Program source
~~~~
